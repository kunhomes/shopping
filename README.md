# shopping

#### 介绍
畅购，基于微服务构架的商城项目，项目采用springboot+springcloud作为底层架构，系统之间调用 eurake服务来实现微服务之间的通信，feign作为高层http调用工具。首页搜索系统搭建es集群，采用Spring security + Oauth2完成用户认证及用户授权，使用spring cloud gateway实现过滤及路由，主要功能模块有商品微服务，广告微服务，搜索微服务，订单微服务，权限微服务，用户微服务，秒杀微服务，管理员后台微服务，rabbitMq微服务，支付微服务等等。

#### 软件架构
软件架构说明
SpringBoot + SpringCloud+ Spring Data + Eureka + Mysql + Redis + RabbitMQ + Oauth2.0
 
开发环境：IntelliJ IDEA + Maven + Tomcat7 + GIT + JDK1.8


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
